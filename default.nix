{ pkgs ? import <nixpkgs> { } 
, ghcVersion ? "ghc944"
}:
let
  haskell = pkgs.haskell.compiler.${ghcVersion};
  haskellPackages = pkgs.haskell.packages.${ghcVersion};
in haskellPackages.mkDerivation rec {
  pname = "not-church-discord-bot";
  version = "0.0.1";

  src = ./.;

  executableHaskellDepends = with haskellPackages; [
    discord-haskell
    unliftio
    text
  ];

  license = "MIT";
}