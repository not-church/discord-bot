## Building and Running

Run the following command to build the project. Note that the first build will take a while, Calamity has a pretty heavy dependency footprint. Subsequent builds should be significantly faster.

```sh
$ nix-build
```

And to run:

```sh
$ nix-build
```

Note that `cabal run` will build and run, so you don't need to run `cabal build` every time.

To open a GHCi session in the context of the project:

(this won't work yet)

```sh
$ nix-shell --command "cabal repl"
```